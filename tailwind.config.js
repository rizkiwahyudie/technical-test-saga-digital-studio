/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'yellow-main': 'rgba(241, 203, 128, 1)',
        'blue-main': 'rgba(41, 159, 255, 1)',
        'blue-soft': 'rgba(96, 174, 238, 1)',
        'purple-soft': 'rgba(178, 127, 185, 1)',
        'gray-main': 'rgba(246, 246, 246, 1)',
        'gray-soft': 'rgba(212, 212, 212, 1)',
        'gray-subtitle': 'rgba(101, 98, 99, 1)',
        'black-border': 'rgba(35, 31, 32, 1)',
      },
      screens: {
        'xs': '465px',
      },
    },
  },
  plugins: [],
}